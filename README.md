Este proyecto esta creado con las siguientes tecnologías:

1- React.js.
2- Webpack.js
3- Babel.
4- ECMAScript.
5- Node.js

DEPENDENCIAS:

1- React: Incialmente fue una librería de JavaScript creado por Facebook, a medida que fue avanzando y su demanda ahora es un Framwork.
2- React-dom: El paquete react-dom proporciona métodos específicos del DOM que pueden ser utilizados en el nivel más alto de tu aplicación como una vía de escape del modelo de React si así lo necesitas.

DevDependencias:

1- Webpack: Paquete de módulos de JavaScript de código abierto.
2- Webpack-cli: Herramientas o línea de comandos para la consola. 
3- babel-loader: Plugin de Webpack para poder trabajar con Babel.
4- @babel/core: Este es el núcleo como tal del babel-loader.
5- @babel/preset-env: Plugin o preset que nos permitirá trabajar con ECMAScript para que nuestro código sea compatible con todos los navegadores.
6- @babel/preset-react: Plugin o preset que nos permitirá trabajar con react.

Flujo Branch: Este proyecto trabaja bajo la rama de Master para un entorno de Producción y Developer para un entorno de desarrollo.

Entorno Desarrollo: Para poder correr este proyecto en local, debemos clonar el repo y con la consola iniciar npm install para instalar todas las dependecias o modulos que se estará usando.

